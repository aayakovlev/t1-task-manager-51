clean:
	mvn clean

install:
	mvn install

install-no-tests:
	mvn install -DskipTests

clean-install:
	mvn clean install

clean-install-no-tests:
	mvn clean install -DskipTests

db-up:
	docker run -d \
    	--rm \
    	--name tm-db \
    	-e POSTGRES_PASSWORD=postgres \
    	-e POSTGRES_DB=task_manager \
    	-p 35432:5432 \
    	postgres:14-alpine

init-scheme:
	cd tm-server && mvn liquibase:update

db-down:
	docker stop tm-db

mongo-up:
	docker run -d \
	--rm \
	--name tm-mongo \
	-p 27017:27017 \
	-v ./data:/data/db:Z \
	mongo:latest

mongo-down:
	docker stop tm-mongo

gitlab-runner-up:
	docker run -d \
	  --name gitlab-runner \
	  --restart always \
	  -v config:/etc/gitlab-runner \
	  -v //var/run/docker.sock:/var/run/docker.sock \
	  gitlab/gitlab-runner:latest

gitlab-runner-down:
	docker stop gitlab-runner
