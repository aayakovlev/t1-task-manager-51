package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.service.dto.ProjectDTOService;
import ru.t1.aayakovlev.tm.service.dto.ProjectTaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.TaskDTOService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;

public interface ServiceLocator {

    @NotNull
    AuthService getAuthService();

    @NotNull
    DomainService getDomainService();

    @NotNull
    LoggerService getLoggerService();

    @NotNull
    ProjectDTOService getProjectService();

    @NotNull
    ProjectTaskDTOService getProjectTaskService();

    @NotNull
    PropertyService getPropertyService();

    @NotNull
    TaskDTOService getTaskService();

    @NotNull
    UserDTOService getUserService();

}
