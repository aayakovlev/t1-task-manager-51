package ru.t1.aayakovlev.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.*;
import ru.t1.aayakovlev.tm.dto.response.*;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ProjectEndpoint extends BaseEndpoint {

    @NotNull
    String NAME = "ProjectEndpointImpl";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ProjectEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ProjectEndpoint newInstance(@NotNull final ConnectionProvider connectionProvider) {
        return BaseEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, ProjectEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ProjectEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return BaseEndpoint.newInstance(host, port, NAME, SPACE, PART, ProjectEndpoint.class);
    }

    @NotNull
    @WebMethod
    ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectClearResponse clearProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectClearRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCompeteByIdResponse completeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCompleteByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectCreateRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectRemoveByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectShowAllResponse showAllProjects(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowAllRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectShowByIdResponse showProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectShowByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectStartByIdResponse startProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectStartByIdRequest request
    ) throws AbstractException;

    @NotNull
    @WebMethod
    ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ProjectUpdateByIdRequest request
    ) throws AbstractException;

}
