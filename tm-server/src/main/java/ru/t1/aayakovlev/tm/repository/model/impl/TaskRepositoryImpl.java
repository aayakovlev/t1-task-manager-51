package ru.t1.aayakovlev.tm.repository.model.impl;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.comparator.NameComparator;
import ru.t1.aayakovlev.tm.comparator.StatusComparator;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class TaskRepositoryImpl extends AbstractExtendedRepository<Task> implements TaskRepository {

    public TaskRepositoryImpl(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<Task> getClazz() {
        return Task.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";

    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Task task = new Task();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        return save(task);
    }

    @NotNull
    @Override
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        @NotNull final Task task = new Task();
        task.setUser(entityManager.find(User.class, userId));
        task.setName(name);
        task.setDescription(description);
        return save(task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.user_id = :userId and e.project_id = :projectId";
        return entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String query = "delete from " + getClazz().getSimpleName() + " e " +
                "where e.user_id = :userId and e.project_id = :projectId";
        entityManager.createQuery(query, getClazz())
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
