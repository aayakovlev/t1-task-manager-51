package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.endpoint.*;

public interface ServiceLocator {

    @NotNull
    AuthEndpoint getAuthEndpoint();

    @NotNull
    CommandService getCommandService();

    @NotNull
    DomainEndpoint getDomainEndpoint();

    @NotNull
    LoggerService getLoggerService();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    PropertyService getPropertyService();

    @NotNull
    SystemEndpoint getSystemEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    TokenService getTokenService();

    @NotNull
    UserEndpoint getUserEndpoint();

}
