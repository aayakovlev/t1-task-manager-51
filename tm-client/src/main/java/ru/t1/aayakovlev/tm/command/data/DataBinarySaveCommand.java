package ru.t1.aayakovlev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.DataBinarySaveRequest;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-bin-save";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() throws AbstractException {
        System.out.println("[DATA SAVE BINARY]");
        getDomainEndpoint().binarySave(new DataBinarySaveRequest(getToken()));
    }

}
